#include "server_p.h"

static void usage(){
  extern char * __progname;
  fprintf(stderr, "usage: %s portnumber directory logfile\n", __progname);
  exit(1);
}

static void kidhandler(int signum) {
  /* signal handler for SIGCHLD */
  waitpid(WAIT_ANY, NULL, WNOHANG);
}

int main(int argc, char *argv[])
{
  struct sockaddr_in sockname, client;
  char *ep;
  struct sigaction sa;
  int clientlen, sd;
  u_short port;
  pid_t pid;
  u_long p;
  int len, rc;
	FILE* logfd;

  /* first, figure out what port we will listen on
   */
  if(argc != 4)
    usage();
  errno = 0;
  
  p = strtoul(argv[1], &ep, 10);
  if(*argv[1] == '\0' || *ep != '\0') {
    /* parameter wasn't a number or was empty */
    fprintf(stderr, "%s - not a number\n", argv[1]);
    usage();
  }

  /* Now check if the directory is okay */
  len = strnlen(argv[2], MAXARGLEN+1);
  if(len > MAXARGLEN){
    fprintf(stderr, "directory path is too long (%d, max %d)\n", len, MAXARGLEN);
    usage();
  }

  /* Change directory what buddy says */
  if(chdir(argv[2]) == -1){
    // Chdir returns -1 if it couldn't change the working directory

    // Should I error out or just leave it as here? - Easier to error out
    fprintf(stderr, "%s - invalid directory\n", argv[2]);
    usage();
  }

  /* Now check if the logfile is okay */
  len = strnlen(argv[3], MAXARGLEN + 1);
  if(len > MAXARGLEN){
    fprintf(stderr, "logfile name is too long (%d, max %d)\n", len, MAXARGLEN);
    usage();
  }
	// Open the file
	if( (logfd = fopen(argv[3], "a")) == NULL)
	    err(1, "couldn't open %s for writing.\n", argv[3]);
  
  if((errno == ERANGE && p == ULONG_MAX) || (p > USHRT_MAX)) {
    /* It's a number, but it either can't fit into an unsigned
     * long, or is too big for an unsigned short. */
    fprintf(stderr, "%s - value out of range\n", argv[1]);
    usage();
  }
  /* now safe to do this */
  port = p;

  memset(&sockname, 0, sizeof(sockname));
  sockname.sin_family = AF_INET;
  sockname.sin_port = htons(port);
  sockname.sin_addr.s_addr = htonl(INADDR_ANY);
  sd = socket(AF_INET, SOCK_STREAM, 0);

  if(sd == -1)
    err(1, "socket failed");

  if(bind(sd, (struct sockaddr *) &sockname, sizeof(sockname)) == -1)
    err(1, "bind failed");

  if(listen(sd,3) == -1)
    err(1, "listen failed");

  /* 
   * We've now bound, and are listening for connections of sd - each
   * call to accept will return us a descriptor talking to a connected
   * client.
   */

  /*
   * First, let's make sure we can have children without leaving zombies
   * around when they die - we can do this by catching SIGCHLD
   */
  sa.sa_handler = kidhandler;
  sigemptyset(&sa.sa_mask);
  /*
   * we want to allow system calls like accept to be restarted if they
   * get interrupted by a SIGCHLD
   */
  sa.sa_flags = SA_RESTART;
  if(sigaction(SIGCHLD, &sa, NULL) == -1)
    err(1, "sigaction failed");

	pthread_t thread;
		    int clientsd;
	/* Loop de loop */
	while(1) {
	    clientlen = sizeof(&client);
	    clientsd = accept(sd, (struct sockaddr *)&client, &clientlen);
	    if(clientsd == 1)
		err(1, "accept failed");	 
	    struct thread_data data = {clientsd, logfd};
		if(pthread_create(&thread, NULL, handle_connection, &data) != 0)
		    fprintf(stderr, "Failed to create new thread");
	}
	close(clientsd);
	return;
}

/* 
 * Called by every fork() with the socket descriptor of the new client.
 * Performs the actual read to get the request, and the write of the file to
 * the client.
 */
void* handle_connection(void* stuff){
	char * file;

	FILE* logfd;
	int clientsd;

	struct thread_data *my_data;

	my_data = (struct thread_data*) stuff;
	logfd = my_data->logfd;
	clientsd = my_data->clientsd;
	// Read the Request from the client and save the file in file
	file = read_request(clientsd);
  
	// Check if the file was null
	if(file == NULL){
	    printf("Invalid file\n");
	    return;
	}
  
	// open the file
	FILE* fd;
	// Need to have a check for if it was permissions or File not found
	if( (fd = fopen(file, "r")) == NULL){
	  if(DEBUG) printf("couldn't open file: %s for reading.\n", file);
	    // write to socket that we couldn't find or open the file
	    // Currently going to assume that we couldn't open it since it didn't exist
	    write_to_file(clientsd, make_reply(file_not_found_header,
		file_not_found_body));
	    return; // We don't want to try to write to it now
	}
  	// give the file to write_to_client
	write_to_client(clientsd, fd);

	// Close the file
	if(close(clientsd));

	// write to log
	log_file(logfd, "Hello!", "LOGGS", 1, 2, 3, &mymutex);
// void log_file(FILE*, char*, char*, int, int, int);

	// Don't close this now, other ppl still need it.
	/*if(fclose(logfd) != 0)
	  err(1, "Failed to close logfile");*/
	printf("Done with this thread\n");
}

/*
 * Writes the file to the client.
 */
void write_to_client(int clientsd, FILE* fd){
	ssize_t written, w;
	char write_buffer[WRITE_BUFFER_SIZE];

	int first_write = 1;
	if(DEBUG) printf("going to write to the client now!\n");
	
	// Need to do something here so I am actually using HTTP protocol
	while(fgets(write_buffer, WRITE_BUFFER_SIZE, fd)){
	  
	  /*
	   * Write the message to the client, being sure to handle a short
	   * write, or being interrupted by a signal before we could write
	   */
	  if(first_write){
		write_to_file(clientsd, make_successful_reply(write_buffer));
		first_write = 0;
	  } else {
		
		write_to_file(clientsd, write_buffer);
	  }
	}

	/* Going to assume that if fgets returned NULL on the first run throught that
	 * the file was just blank
	 */
	if(first_write == 1 && *write_buffer == '\0')	
	    write_to_file(clientsd, make_successful_reply("\0"));

      	if(DEBUG) printf("We done writing now: %s\n", write_buffer);
	
}

/*
 * Reads the request from the client and writes the appropriate parts to disk.
 * 
 * Uses stuff from Bob's filewrite-C.c example for file writing
 * Uses stuff from Bob's iagree.c example for proper reading
 */
char* read_request(int clientsd){
	ssize_t r, rc, maxread;
	char read_buffer[REQUEST_SIZE];
	char* get_position;

	r = -1;
	rc = 0;

	maxread = REQUEST_SIZE - 1;

	// We only care about first line
	printf("About to read the first line!\n"); 
	while((r != 0) && rc < maxread) {
	    r = read(clientsd, read_buffer + rc, maxread - rc);
	    if( r == -1){
		if(errno != EINTR)
		    err(1, "read failed");
		} else
		    rc += r;

	    /*
	     * Check if command starts with GET otherwise its a bad request
	     * and we can exit
	     */
	    if(!(get_position = strstr(read_buffer, "GET"))){
		printf("No GET in the string\n");
		write_to_file(clientsd, make_reply(bad_request_header,
		    bad_request_body)); // Say how bad it was
		return NULL;
	    }

	    if(get_position != read_buffer){
		/* We had a GET but not at beginning */
		printf("GET wasn't at the beginning bad boys\n");
		write_to_file(clientsd, make_reply(bad_request_header,
		    bad_request_body));
		return NULL;
	    }

	    /*
	     * We keep reading until we hit two newlines signifying
	     * The end of an HTTP thing.
	     */
	    
	    if(strstr(read_buffer, "\r\n") != NULL){
	      if(DEBUG) printf("Hit a double newline, we can stop reading now\n");
	        break;
	    }
	}

	/* Now we chop off buffer at first newline, and we're
	 * certain that we have at least one NEWLINE
	 */

	char* newline = strchr(read_buffer, '\n');
	if(newline != NULL){
		/* We have the first line, we can null
		 * char it here and leave the loop
		 */
	    *newline = '\0'; // Truncate read_buffer here
	}
	

	if(DEBUG) {printf("Finished reading request, read %zd characters.\n", rc);
	  printf("First line is: %s\n", read_buffer);}
   

	// For now just return the the file here, later on somewhere else
	char* temp =  get_request_to_file(read_buffer);
	if(temp == NULL){
	  // Write to client error
	  // write_to_file(clientsd, bad_request_response);
	  write_to_file(clientsd, make_reply(bad_request_header,
		bad_request_body));
	  return NULL;
	}

	return temp;
}

/*
 * Takes a GET request and returns the name it was requesting
 * Returns the file or NULL if there is any issues
 */
char* get_request_to_file(char* request){
  // Double check to make sure it's an actual GET request
  if(strstr(request, "GET") == NULL)
    return NULL;

  // Make sure it contained at least the leading /
  if(strstr(request, "GET /") == NULL)
    return NULL;

  // Trim request up to after first /
  char * file = malloc(sizeof(char)*strlen(request) - 4);
  if(file == NULL)
    err(1, "Malloc failed");
  strncpy(file, request + 5, strlen(request) - 4);

  // Trim whitespace on the end
  char* end = strchr(file, ' ');
  if(end == NULL){
    // No space!
    return NULL;
  } else {
   * end = '\0';
  }

  // Should be fine to return request now
  return file;
}

/*
 * Writing to a file, stolen from Bob's filewrite-POSIX.c example
 * 
 * Write "text" to the file using write
 */
void write_to_file(int fd, char* text){
  	// Write to the file, checking for interrupted writes
	int len, rc, written;
	len = strnlen(text, REQUEST_SIZE+1);
	if(len > REQUEST_SIZE){
	    fprintf(stderr, "Request to long to write to file\n");
    
	    // Should we be exiting here?
	    // exit(-1); 
	}

	if(DEBUG) printf("Going to write to the file now\n");

	printf("Status of fd is:%d\n", fd);

	if(DEBUG) printf("Going to write this to the client now:\n%s\n", text);
	
	for(written = 0; written < len; written += rc){
	    if( (rc = write(fd, text + written, len - written)) == -1){
	      if(errno != EINTR)
		err(1, "write failed, only wrote %d of %d chars", written, len);
	      else
		rc = 0;
	    }
  	}
	if(DEBUG) printf("Finished writing\n");
}
