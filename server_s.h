
/*
 * Copyright (c) 2008 Bob Beck <beck@obtuse.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * select_server.c - an example of using select to implement a non-forking
 * server. In this case this is an "echo" server - it simply reads
 * input from clients, and echoes it back again to them, one line at
 * a time.
 *
 * to use, cc -DDEBUG -o select_server select_server.c
 * or cc -o select_server select_server.c after you read the code :)
 *
 * then run with select_server PORT
 * where PORT is some numeric port you want to listen on.
 *
 * to connect to it, then use telnet or nc
 * i.e.
 * telnet localhost PORT
 * or
 * nc localhost PORT
 * 
 */


#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include <netinet/in.h>

#include <err.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "replies.h"

static void usage();
struct con * get_free_conn();
void closecon (struct con *cp, int initflag);
void handlewrite(struct con *cp);
void handleread(struct con *cp);
void badrequest(struct con *cp);


/* we use this structure to keep track of each connection to us */
struct con {
	int sd; 	/* the socket for this connection */
	int state; 	/* the state of the connection */
	struct sockaddr_in sa; /* the sockaddr of the connection */
	size_t  slen;   /* the sockaddr length of the connection */
	char *buf;	/* a buffer to store the characters read in */
	char *bp;	/* where we are in the buffer */
	size_t bs;	/* total size of the buffer */
	size_t bl;	/* how much we have left to read/write */
};

/*
 * we will accept a maximum of 256 simultaneous connections to us.
 * While you could make this a dynamically allocated array, and
 * use a variable for maxconn instead of a #define, that is left
 * as an exercise to the reader. The necessity of doing this
 * in the real world is debatable. Even the most monsterous of
 * daemons on real unix machines can typically only deal with several
 * thousand simultaeous connections, due to limitations of the
 * operating system and process limits. so it might not be worth it
 * in general to make this part fully dynamic, depending on your
 * application. For example, there is no point in allowing for
 * more connections than the kernel will allow your process to
 * have open files - so run 'ulimit -a' to see what that is as
 * an example of a typical reasonable number, and bear in mind
 * you may have a few more files open than just your sockets
 * in order to do anything really useful
 */
#define MAXCONN 256
struct con connections[MAXCONN];

#define REQUEST_SIZE 512
#define MAXARGLEN 80
#define WRITE_BUFFER_SIZE 4096
 
#define BUF_ASIZE 256 /* how much buf will we allocate at a time. */

/* states used in struct con. */
#define STATE_UNUSED 0
#define STATE_READING 1
#define STATE_WRITING 2
