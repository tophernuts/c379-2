#ifndef REPLIES_H
#define REPLIES_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define REPLY_DEBUG 1

char* make_successful_reply(char*);
char* make_reply(char *, char*);
char* getDate();

// Don't define these in here, just have their declartions
/* HTTP response for successful request. */
static char* successful_response_header =
  "HTTP/1.1 200 OK\n";
static char* successful_response_body =
  "Content-type: text/html\n"
  "Content-Length: 1234\n"
  "\n";

/* HTTP response, header, and body for bad response (everything but GET)*/
static char* bad_request_header = 
  "HTTP/1.1 400 Bad Request\n";
static char* bad_request_body =
  "Content-type: text/html\n"
  "\n"
  "<html>\n"
  " <body>\n"
  "  <h1>Bad Request</h1>\n"
  "  <p>This server did not understand your request.</p>\n"
  " </body>\n"
  "</html>\n";

static char * file_not_found_header = 
  "HTTP/1.1 404 Not Found";
static char * file_not_found_body =
  "Content-Type: text/html\n"
  "Content-Length: 117\n"
  "\n"
  "<html><body>\n"
  "<h2>Document not found</h2>\n"
  "You asked for a document that doesn't exist.\n"
  "</body></html>";

static char* file_forbidden_header =
  "HTTP/1.1 403 Forbidden\n";
static char* file_forbidden_body =
  "Content-Type: text/html\n"
  "Content-Length: 130\n\n"
  "<html><body>\n"
  "<h2>Permission Denied</h2>\n"
  "You asked for a document you do not have permissions to view.\n"
  "</body></html>";

static char* server_error_header = 
  "HTTP/1.1 500 Internal Server Error\n";
static char* server_error_body = 
  "Content-Type: text/html\n"
  "Content-Length: 131\n\n"
  "<html><body>\n"
  "<h2>Oops. That didn't work.</h2>\n"
  "I had some sort of problem dealing with the request.\n"
  "</body></html>";
#endif
