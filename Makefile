all: server_f server_p server_s

server_f: server_f.o replies.o log.o
	gcc -g server_f.o replies.o log.o -o server_f

server_p: server_p.o replies.o log.o
	gcc -g -pthread server_p.o replies.o log.o -o server_p

server_s: server_s.o replies.o log.o
	gcc -g -pthread server_s.o replies.o log.o -o server_s

server_f.o: server_f.c
	gcc -c server_f.c

server_s.o: server_s.c
	gcc -c server_s.c

server_p.o: server_p.c
	gcc -c server_p.c

replies.o: replies.c
	gcc -c replies.c

log.o: log.c
	gcc -c log.c
 	
clean:
	rm -rf server_f server_p *.o
