#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <pthread.h>
#include "replies.h"
#include "server_p.h"

char* create_log(long, char*, char*, int, int, int);
char* request_to_text(int, int, int);
int numplaces(long);
void log_file(FILE*, char*, char*, int, int, int, pthread_mutex_t*);

#endif
