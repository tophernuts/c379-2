#include "replies.h"

/* 
 * Returns a successfull reply
 */
char* make_successful_reply(char* message){
	char* body = malloc(strlen(message) + strlen(successful_response_body));
	strncpy(body, successful_response_body,
	    strlen(successful_response_body));
	strncat(body, message, strlen(message));
	strcat(body, "\0");
	if(REPLY_DEBUG) printf("Going to write_to_file with: \n%s\n", body);
	return make_reply(successful_response_header, body);
}

/*
 * Returns an appropriate reply based on the header and body passsed to it
 */
char* make_reply(char* header, char* body){
	char * date = getDate();
	ssize_t size = strlen(header) + strlen(date) + strlen(body);
	char * reply = malloc(size);

	strncpy(reply, header, strlen(header));
	strncat(reply, date, strlen(date));
	strncat(reply, body, strlen(body));
	strcat(reply, "\0");

	if(reply == NULL)
	    err(1, "malloc failed");
	return reply;
}
/* Returns the current date,
 * Stolen from: http://en.wikipedia.org/wiki/C_date_and_time_functions*/
char* getDate(){
	time_t current_time;
	char* c_time_string;

	/* Obtain current time as seconds elapsed since epoch */
	current_time = time(NULL);

	if(current_time == ((time_t) - 1)){
	    err(1, "Failure to compute the current time.");
	}

	/* Convert to local time format. */
	c_time_string = ctime(&current_time);

	if(c_time_string == NULL)
	    err(1, "Failure to convert the current time");
	
	return c_time_string;
}
