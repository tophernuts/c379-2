#include "log.h"

/* Shit should look like:
 * Mon, 21 Jan 2008 18:06:16 GMT	129.128.11.43	GET /fluffy.html HTTP/1.0	400 Bad Request
 * Mon, 21 Jan 2008 18:06:16 GMT	129.128.11.43	GET /fluffy.html HTTP/1.1	200 OK 1435/1435
 * Mon, 21 Jan 2008 18:06:23 GMT	198.103.98.25	GET /something.html HTTP/1.1	403 Forbidden
 * Mon, 21 Jan 2008 18:18:20 GMT	129.128.41.238	GET /noexist.html HTTP/1.1	404 Not Found
 * Mon, 21 Jan 2008 18:18:45 GMT	12.110.110.204	GET /goodstuff.html HTTP/1.1	500 Internal Server Error
 * Mon, 21 Jan 2008 18:29:33 GMT	129.128.11.43	GET /silly.html HTTP/1.1	200 OK 4096/11435
 */


/*
 * Logs the request
 *
 * http://bofh.srv.ualberta.ca/beck/c379/examples/filewrite-C.c
 */
void log_file(FILE* fd, char* IP, char* request, int code, int delivered, int sent, pthread_mutex_t *mymutex){
	// Will eventually have locks here and stuff
	char* log_text = create_log(0, IP, request, code, delivered, sent);	


	printf("Going to log this: %s\n", log_text);
	// write to the file
	int rc, len;
	len = strlen(log_text) + 1; // Don't forget the newline!!
//	pthread_mutex_lock(&mymutex);
	if( (rc = fprintf(fd, "%s\n", log_text)) != len){
	    fprintf(stderr, "write failed, wrote %d of %d chars,\n", rc, len);
		// pthread_mutex_unlock(&mymutex); // Don't want a deadlock
		exit(-1);
	}
	//pthread_mutex_unlock(&mymutex);
}

char* create_log(long epoch, char* ip_addr, char* command, int response,
	int requested, int delivered){
	// Should this use the current date or the date passed to it
	char* date = getDate();
	char* reply = request_to_text(response, requested, delivered);
	char* log = malloc(strlen(date) + strlen(ip_addr) + strlen(command) +
	    strlen(reply) + strlen("\t")*3);
	if(sprintf(log, "%s\t%s\t%s\t%s", date, ip_addr, command, reply) == -1)
	    err(1, "sprintf failed");
	return log;
}

char* request_to_text(int code, int requested, int delivered){
	char* ret;

	switch(code){
	    case 200:
		ret = malloc(sizeof(char)*(numPlaces(requested) +
		    numPlaces(delivered) + 8));
		if(sprintf(ret, "200 OK %d/%d", requested, delivered) == -1)
		    err(1, "sprintf failed");
		break;
	    case 400:
		ret = "400 Bad Request";		
		break;
	    case 403:
		ret = "403 Forbidden";
		break;
	    case 404:
		ret = "404 Not Found";
		break;
	    case 500:
		ret = "500 Internal Server Error";
		break;
	    default:
		ret = malloc(sizeof(char)*(numPlaces(code) + 15));
		if(sprintf(ret, "%d Unknown Code", code) == -1)
		    err(1, "sprintf failed");
		break;
	}
	return ret;
}

/*
 * Stolen from: http://stackoverflow.com/a/1068937/1684866
 */
int numPlaces (int n) {
    if (n < 0) return numPlaces ((n == INT_MIN) ? INT_MAX : -n);
    if (n < 10) return 1;
    return 1 + numPlaces (n / 10);
}
